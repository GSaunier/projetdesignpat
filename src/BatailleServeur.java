import model.Bataille;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;

public class BatailleServeur {
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        System.out.println("Construction du Serveur Bataille ... ");
        Bataille jeu = new Bataille(new HashMap<Integer, Integer>());

        System.out.println("Cration du registre pour distribution...");
        Registry registry = LocateRegistry.createRegistry(9898);
        //registry.bind("central_bataille",jeu);

        System.out.println("En attente des messages clients...");
    }
}
