package view;

import model.Bataille;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class VueDebut extends JFrame {
    public VueDebut() {
        this.setLayout(new BorderLayout());
        JButton bSolo = new JButton("Solo");
        bSolo.setPreferredSize(new Dimension(100,100));
        JFrame f = this;
        bSolo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Bataille mod = new Bataille(new HashMap<>());
                VueGenerale vg = new VueGenerale(mod);
                f.setVisible(false);

            }
        });
        JButton bMulti = new JButton("Multi");
        bMulti.setPreferredSize(new Dimension(100,100));

        this.add(bSolo, BorderLayout.CENTER);
        this.add(bMulti, BorderLayout.EAST);


        this.setTitle("Bataille navale");
        this.setSize(200, 100);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setLocationRelativeTo(null);

        this.setVisible(true);

    }

}
