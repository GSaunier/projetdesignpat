package view;


import model.Bataille;
import model.Etat;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * Created by Utilisateur on 07/11/2018.
 */
public class VueGenerale extends JFrame implements Observer {
    private VuePlateau vpJoueur;
    private VuePlateau vpMachine;
    private VueCentrale vc;
    private VueEpoque ve;
    private VueMenu vm;
    private Bataille mod;
    private JPanel princ;
    private VueHisto vh;
    private Etat e;

    public VueGenerale(Bataille mod) {
        mod.addObserver(this);
        this.mod = mod;
        e = mod.getEtat();
        ve = new VueEpoque(mod);
        vh = new VueHisto(mod);

        princ = new JPanel();
        princ.setLayout(new BorderLayout());

        vm = new VueMenu(mod, this);

        vpJoueur = new VuePlateau(mod,"joueur");
        vpMachine = new VuePlateau(mod,"machine");
        vc = new VueCentrale(mod);

        princ.add(vpJoueur, BorderLayout.EAST);
        princ.add(vc,BorderLayout.NORTH);
        princ.add(vpMachine, BorderLayout.WEST);

        this.setLayout(new BorderLayout());
        this.add(ve, BorderLayout.CENTER);
        this.add(vh,BorderLayout.EAST);

        this.setTitle("Bataille navale");
        this.setSize(2*VuePlateau.DIMENSION + 320, VuePlateau.DIMENSION+80);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setLocationRelativeTo(null);

        this.setVisible(true);
    }

    public void addJeu() {


        e = mod.getEtat();
        this.remove(ve);
        this.add(princ);
        this.setJMenuBar(vm);
        revalidate();
        repaint();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (e != mod.getEtat() && mod.getEtat() != Etat.EPOQUE)
            addJeu();
        else if (e != mod.getEtat() && mod.getEtat() == Etat.EPOQUE)
            vueEp();
    }

    public void vueEp() {
        e = mod.getEtat();
        this.setJMenuBar(null);
        this.remove(princ);
        this.add(ve);
        this.setJMenuBar(vm);
        revalidate();
        repaint();
    }
}
