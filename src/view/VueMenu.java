package view;


import DAO.DaoFactoryCSV;
import model.Bataille;
import model.Etat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Utilisateur on 07/11/2018.
 */
public class VueMenu extends JMenuBar implements Observer {
    private final VueGenerale frame;
    private Bataille mod;
    JMenuItem rejouer = new JMenuItem("Rejouer");
    JMenuItem save = new JMenuItem("Sauvegarder");
    JMenuItem quit = new JMenuItem("Quitter");

    public VueMenu(Bataille mod, VueGenerale f) {
        this.frame = f;
        mod.addObserver(this);
        this.mod = mod;

        JMenu jm = new JMenu("Fichier");
        jm.add(rejouer);
        jm.add(save);
        jm.add(quit);
        rejouer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mod.initialiser();
            }
        });

        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                int returnVal = chooser.showOpenDialog(null);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    System.out.println("You chose to open this file: " +
                            chooser.getSelectedFile().getName());
                    DaoFactoryCSV.getInstance().getJeuDao().save(chooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });


        this.add(jm);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (mod.getEtat() == Etat.ENJEU){
            save.setEnabled(true);
        }

    }
}
