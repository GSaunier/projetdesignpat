package view;

import model.Bataille;
import model.historique.Historique;
import model.plateau.Bateau;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class VueHisto extends JPanel implements Observer {

    private Bataille mod;
    private JList<String> jList;
    private DefaultListModel<String> list;

    public VueHisto(Bataille m) {
        list = new DefaultListModel<>();
        jList =new JList<>(list);
        mod = m;
        mod.addObserver(this);
        this.setPreferredSize(new Dimension(300, VuePlateau.DIMENSION+80));
        jList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        jList.setLayoutOrientation(JList.VERTICAL);
        jList.setVisibleRowCount(1);
        JScrollPane listScroller = new JScrollPane(jList);
        JLabel jLab = new JLabel("Historique");
        jLab.setPreferredSize(new Dimension(400,38));
        jLab.setHorizontalAlignment(SwingConstants.CENTER);
        jLab.setVerticalAlignment(SwingConstants.CENTER);

        this.setLayout(new BorderLayout());
        this.add(jLab, BorderLayout.NORTH);
        this.add(listScroller,BorderLayout.CENTER);
    }


    @Override
    public void update(Observable o, Object arg) {
        if(list.getSize() < Historique.getInstance().getActions().size()) {
            for (String s : Historique.getInstance().getHisto(list.getSize())) {
                list.add(0,s);
            }
        } else if (list.getSize() == Historique.getInstance().getActions().size()){

        } else {
            list.clear();
        }
    }
}
