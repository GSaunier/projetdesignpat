package view;

import control.EcouteurBoutonPlateau;
import model.Bataille;
import model.Etat;
import model.plateau.Bateau;
import model.plateau.Coordonnees;
import model.plateau.Direction;
import model.plateau.Tir;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class VuePlateau extends JPanel implements Observer {
    private Bataille mod;
    private JButton[][] plateau;
    private JButton seletion;
    private String type;
    public static int DIMENSION = 450;
    public static int DIMBUT = DIMENSION/10;

    private ImageIcon[] bateauDroit = new ImageIcon[3];
    private ImageIcon[] bateauGauche = new ImageIcon[3];
    private ImageIcon[] bateauBas = new ImageIcon[3];
    private ImageIcon[] bateauHaut = new ImageIcon[3];
    private ImageIcon eau;


    public VuePlateau(Bataille mod, String type) {
        this.type = type;
        mod.addObserver(this);
        this.mod = mod;

        try {
            bateauBas[0] = new ImageIcon(ImageIO.read(  getClass().getResource("/res/bBDeb.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauBas[1] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bBMid.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauBas[2] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bBFin.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauGauche[0] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bGDeb.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauGauche[1] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bGMid.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauGauche[2] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bGFin.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauDroit[0] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bDeb.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauDroit[1] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bMid.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauDroit[2] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bFin.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauHaut[0] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bHDeb.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauHaut[1] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bHMid.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            bateauHaut[2] = new ImageIcon(ImageIO.read(getClass().getResource("/res/bHFin.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
            eau = new ImageIcon(ImageIO.read(getClass().getResource("/res/eau.png")).getScaledInstance(DIMBUT,DIMBUT,Image.SCALE_DEFAULT));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setPreferredSize(new Dimension(DIMENSION,DIMENSION));

        this.setLayout(new GridLayout(mod.getLargeur(),mod.getHauteur()));

        plateau = new JButton[mod.getLargeur()][mod.getHauteur()];
        for(int i = 0; i < plateau.length; i++) {
            for (int j = 0; j < plateau[0].length; j++){
                JButton b = new JButton();
                b.setSize(DIMENSION/10,DIMENSION/10);
                b.setBackground(Color.decode("#0495FF"));
                if (type.equals("machine"))
                    b.addActionListener(new EcouteurBoutonPlateau(new Coordonnees(j,i),mod,"machine"));
                else
                    b.addActionListener(new EcouteurBoutonPlateau(new Coordonnees(j,i),mod,"joueur"));
                this.add(b);
                plateau[j][i] = b;
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (!mod.isRejouer()) {
            if (type.equals("joueur")) {
                updateJoueur();
            } else
                updateMachine();
        }
    }

    public void updateJoueur() {
        Tir[][] map = mod.getToucher("machine");
        for (int i = 0; i <10; i++) {
            for (int j = 0; j < 10; j++) {
                plateau[i][j].setIcon(null);
                if(map[i][j] == null) {
                    plateau[i][j].setIcon(eau);
                }else if(map[i][j].isToucher() == null)
                    plateau[i][j].setBackground(Color.decode("#FF1917"));
                else if (map[i][j].isToucher())
                    plateau[i][j].setBackground(Color.decode("#FF990A"));
                else
                    plateau[i][j].setBackground(Color.decode("#28FF73"));
                if(mod.getEtat() == Etat.PREPA)
                    plateau[i][j].setEnabled(true);
                else if (mod.getEtat() == Etat.ENJEU)
                    plateau[i][j].setEnabled(true);
                plateau[i][j].setText("");
            }
        }

        for (Bateau b : mod.getBateauJoueur()) {
            int i = 0;
            for (Coordonnees c : b.getPosBateau()) {
                plateau[c.getX()][c.getY()].setBackground(b.getCouleur());
                ImageIcon icon;
                int index;
                if(i == 0)
                    index = 2;
                else if (i == b.getPosBateau().size()-1)
                    index = 0;
                else
                    index = 1;

                if(b.getDir() == Direction.BAS) {
                    icon = bateauBas[index];
                } else if (b.getDir() == Direction.HAUT)
                    icon = bateauHaut[index];
                else if (b.getDir() == Direction.GAUCHE)
                    icon = bateauGauche[index];
                else
                    icon = bateauDroit[index];

                plateau[c.getX()][c.getY()].setIcon(icon);

                if(b == mod.getBateauSelect())
                    plateau[c.getX()][c.getY()].setBackground(Color.decode("#FF4146"));
                else if(map[c.getX()][c.getY()] != null)
                    plateau[c.getX()][c.getY()].setBackground(Color.decode("#FF990A"));
                if(i == 0 ) {
                    plateau[c.getX()][c.getY()].setMargin(new Insets(0,0,0,0));
                    plateau[c.getX()][c.getY()].setText("<HTML><BODY>M: " +b.getMunition()+" <BR>V: "+ b.getVie() +"<BR>P: " + b.getPuissance()+"</BODY></HTML>");
                }
                plateau[c.getX()][c.getY()].setVisible(true);
                i++;
            }
        }
        if(mod.getEtat()==Etat.PREPA) {
            if (mod.getCaseSelect() != null) {
                for (Coordonnees c : mod.getEmplacement().keySet()) {
                    plateau[c.getX()][c.getY()].setIcon(null);
                    plateau[c.getX()][c.getY()].setBackground(Color.decode("#FF4B43"));
                    plateau[c.getX()][c.getY()].setText("<HTML><BODY>M: " +mod.getEmplacement().get(c).getNbMun()+" <BR>V: "+ mod.getEmplacement().get(c).getVie()+"<BR>P: "+ mod.getEmplacement().get(c).getPuissance() +"</BODY></HTML>");
                    plateau[c.getX()][c.getY()].setMargin(new Insets(0,0,0,0));
                }
            }
        }

    }

    public void updateMachine() {

        Tir[][] map = mod.getToucher("joueur");
        for (int i = 0; i <10; i++) {
            for (int j = 0; j < 10; j++) {
                plateau[i][j].setIcon(null);
                if(map[i][j] == null)
                    plateau[i][j].setIcon(eau);
                else if(map[i][j].isToucher() == null)
                    plateau[i][j].setBackground(Color.decode("#FF1917"));
                else if (map[i][j].isToucher())
                    plateau[i][j].setBackground(Color.decode("#FF990A"));
                else
                    plateau[i][j].setBackground(Color.decode("#28FF73"));
                if(mod.getEtat() == Etat.PREPA)
                    plateau[i][j].setEnabled(true);
                else if (mod.getEtat() == Etat.ENJEU)
                    plateau[i][j].setEnabled(true);
                plateau[i][j].setText("");
            }
        }
        /*
        for (Bateau b : mod.getBateauMachine()) {
            for (Coordonnees c : b.getPosBateau()) {
                plateau[c.getX()][c.getY()].setBackground(Color.decode("#FBFF02"));
                plateau[c.getX()][c.getY()].setIcon(null);
            }
        }*/
    }
}
