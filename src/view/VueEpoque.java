package view;

import DAO.DaoFactoryCSV;
import model.Bataille;
import model.Etat;
import model.epoque.Epoque;
import model.historique.Historique;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VueEpoque extends JPanel {
    private Bataille mod;
    public VueEpoque(Bataille mod) {
        this.mod = mod;
        for(String s : Epoque.getEpoque()){
            JButton b = new JButton(s);
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    mod.setEpoque(s,false);
                }
            });
            this.add(b);
        }

        JButton b = new JButton("Charger une partie");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                int returnVal = chooser.showOpenDialog(null);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    System.out.println("You chose to open this file: " +
                            chooser.getSelectedFile().getName());
                    DaoFactoryCSV.getInstance().getJeuDao().load(chooser.getSelectedFile().getAbsolutePath(), mod);
                }

            }
        });
        this.add(b);

    }
}
