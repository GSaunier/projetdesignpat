package view;

import DAO.DaoFactoryCSV;
import control.EcouteurBoutonJouer;
import model.Bataille;
import model.Etat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class VueCentrale extends JPanel implements Observer {
    private JButton jouer = new JButton("Jouer");
    private JLabel dispo;
    private Bataille mod;

    public VueCentrale(Bataille mod) {
        this.mod = mod;
        this.mod.addObserver(this);
        JPanel disponible = new JPanel();
        dispo = new JLabel();

        disponible.add(dispo,BorderLayout.CENTER);

        jouer.addActionListener(new EcouteurBoutonJouer(mod));
        jouer.setEnabled(false);
        add(jouer, BorderLayout.CENTER);
        add(disponible,BorderLayout.EAST);



        update(null,null);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (!mod.isRejouer()) {
            if (mod.estPret()) {
                jouer.setEnabled(true);
            } else
                jouer.setEnabled(false);
            if (mod.getEtat() == Etat.PREPA) {
                dispo.setText("Nombre de Bateau disponible: " + mod.getBateauDispo() + "  Epoque: " + mod.getEpoque().getNom() + " siecle");
            }
            if (mod.getEtat() == Etat.ENJEU) {
                dispo.setText("Epoque: " + mod.getEpoque().getNom());
                if (mod.getBateauSelect() != null)
                    dispo.setText(dispo.getText() + "    Information Bateau:  Vie: " + mod.getBateauSelect().getVie() + "    Munition: " + mod.getBateauSelect().getMunition() + "    Puissance: " + mod.getBateauSelect().getPuissance());
                remove(jouer);
                revalidate();
                repaint();
            }
        }
    }
}
