package DAO;

public class DaoFactoryCSV extends DaoFactory{
    private static DaoFactoryCSV instance;

    private DaoFactoryCSV(){};

    @Override
    public JeuDAO getJeuDao() {
        return JeuDAOCSV.getInstance();
    }

    public static DaoFactoryCSV getInstance() {
        if(instance == null)
            instance = new DaoFactoryCSV();
        return instance;
    }
}
