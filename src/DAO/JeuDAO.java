package DAO;

import model.Bataille;


public interface JeuDAO {
    public void save(String uri);
    public void load(String uri, Bataille mod);

}
