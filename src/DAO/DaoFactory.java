package DAO;

public abstract class DaoFactory {
    public abstract JeuDAO getJeuDao();
    public static DaoFactory getInstance(String type){
        if(type.equals("csv"))
            return DaoFactoryCSV.getInstance();
        return null;
    }
}
