package DAO;

import model.Bataille;
import model.historique.*;
import model.plateau.Coordonnees;
import model.plateau.Direction;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class JeuDAOCSV implements JeuDAO {
    private static JeuDAOCSV instance;
    private JeuDAOCSV(){};

    public static JeuDAOCSV getInstance() {
        if(instance == null)
            instance = new JeuDAOCSV();
        return instance;
    }

    public void save(String uri) {
        try {
            //FileWriter writer = new FileWriter("Carnet.csv");
            FileWriter writer = new FileWriter(uri);
            String csvSeparator = ",";

            for (ActionStrategie a : Historique.getInstance().getActions()) {
                writer.append(a.getHisto());
                writer.append('\n');
            }
            writer.flush();
            writer.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void load(String uri, Bataille mod) {
        ArrayList<ActionStrategie> action = new ArrayList<>();
        BufferedReader br = null;

        String line = "";
        //séparateur du CSV
        String csvSeparator = ",";
        try {
            br = new BufferedReader(new FileReader(uri));
            int i=0;
            //on boucle sur chaque ligne du fichier
            while ((line = br.readLine()) != null) {
                i++;
                // on récupère la ligne que l'on découpe en fonction du séparateur, on
                // obtient un tableau de chaînes de caractères (pour chaque ligne)
                String[] split = line.split(csvSeparator);
                this.recreerHistorique(split).execute(mod);
            }
            System.out.println(Historique.getInstance().toString());
        mod.update();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Chargement réussi!");
    }

    public ActionStrategie recreerHistorique(String [] line){
        if (line[0].equals("epoque")){
           return new ActionEpoque(line[1]);
        }else if (line[0].equals("placer")){
            Direction d = Direction.HAUT;
            switch (line[4]){
                case "HAUT":
                    d = Direction.HAUT;
                    break;
                case "BAS":
                    d= Direction.BAS;
                    break;
                case "DROITE":
                    d = Direction.DROITE;
                    break;
                case "GAUCHE":
                    d = Direction.GAUCHE;
                    break;
            }

           return new ActionPlacerBateau(
                        Integer.parseInt(line[3]),
                        d,
                        new Coordonnees(Integer.parseInt(line[5]), Integer.parseInt(line[6])),
                        line[1],
                        Integer.parseInt(line[2])
                     );

        }else if (line[0].equals("tirer")){
           return new ActionTirer(
                        new Coordonnees(Integer.parseInt(line[3]), Integer.parseInt(line[4])),
                        line[1],
                        Integer.parseInt(line[2])
                    );
        }else if (line[0].equals("jouer")){
            return  new ActionJouer();
        }
        return null;
    }
}
