package control;

import model.Bataille;
import model.Etat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EcouteurBoutonJouer implements ActionListener {
    private Bataille mod;

    public EcouteurBoutonJouer(Bataille mod) {
        this.mod = mod;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        mod.changeEtat(Etat.ENJEU);
    }
}
