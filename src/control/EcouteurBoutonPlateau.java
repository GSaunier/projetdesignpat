package control;

import model.Bataille;
import model.Etat;
import model.plateau.Coordonnees;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EcouteurBoutonPlateau implements ActionListener {
    private Coordonnees coord;
    private Bataille mod;
    private String type;

    public EcouteurBoutonPlateau(Coordonnees coord, Bataille mod, String type) {
        this.mod = mod;
        this.coord = coord;
        this.type = type;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       if ( type.equals("joueur") || (type.equals("machine") && mod.getBateauSelect() != null) )
        mod.selectCase(coord,type);
       while (mod.getEtat() == Etat.FINMUNUSER)
           mod.selectCase(coord,type);
    }
}
