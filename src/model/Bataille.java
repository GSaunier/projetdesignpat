package model;
import model.epoque.Epoque;
import model.historique.*;
import model.joueurs.Humain;
import model.joueurs.Machine;
import model.plateau.*;
import java.util.*;

public class Bataille extends Observable{
    private Epoque epoque;
    private Humain user;
    private Machine ia;
    private Map<Integer,Integer> bateauDispo;
    private HashMap<Coordonnees, Emplacement> emplacement;
    private int largeur = 10;
    private int hauteur = 10;
    private Etat etat;
    private Coordonnees caseSelect;
    private Bateau bateauSelect;
    private boolean rejouer;

    public void selectCase(Coordonnees coordonnees, String type) {
        if(etat == Etat.PREPA)
            selectCasePrepa(coordonnees,type);
        else if (etat == Etat.ENJEU)
            selectCaseEnJeu(coordonnees,type);
        else if(etat == Etat.FINMUNUSER) {
            if (ia.getNbMunitions() > 0) {
                Coordonnees coordTir = ia.jouer();
                ia.tirer(coordTir);
                update();
            } else {
                etat = Etat.FINI;
                String gagnant;
                int p;
                int g;
                if(user.getPdv() > ia.getPdv()) {
                    gagnant = "Joueur";
                    g = user.getPdv();
                    p = ia.getPdv();
                }else if (user.getPdv() < ia.getPdv()) {
                    gagnant = "Machine";
                    p = user.getPdv();
                    g = ia.getPdv();
                } else {
                    gagnant = "Egalité";
                    g = user.getPdv();
                    p = ia.getPdv();
                }
                Historique.getInstance().addAction(new ActionFin(gagnant,g,p));
            }
        }
        update();
    }

    public void choixJeu(int choix) {
        // Cas jeu solo
        if(choix == 1) {

        } else {

        }
    }

    public void selectCasePrepa(Coordonnees coordonnees, String type) {
        if (user.presenceBateau(coordonnees)) {
            user.retirerBateau(coordonnees);
            emplacement.clear();
        } else if (!emplacement.keySet().contains(coordonnees)) {
            caseSelect = coordonnees;
            calculPosDispo(caseSelect);
        } else if (emplacement.keySet().contains(coordonnees)) {
            Emplacement e = emplacement.get(coordonnees);
            user.placerBateau(e.getNbCase(), caseSelect, e.getDir());
            emplacement.clear();
        }
    }


    public void selectCaseEnJeu(Coordonnees coordonnees, String type) {
        if(type.equals("joueur")) {
            bateauSelect = selectionBateau(coordonnees);
        } else {
            tirer(coordonnees);
        }
    }

    public void tirer(Coordonnees coord) {
        if(bateauSelect.getMunition() > 0 && user.tirer(coord)) {
            bateauSelect.tirer();
            Historique.getInstance().addAction(new ActionTirer(coord,"joueur",bateauSelect.getId()));
            bateauSelect = null;
            if(ia.getNbMunitions()>0) {
                Coordonnees coordTir = ia.jouer();
                if(ia.tirer(coordTir))
                    Historique.getInstance().addAction(new ActionTirer(coordTir,"machine",ia.getBateauSelect().getId()));
            }
            if(user.getNbMunitions() <= 0)
                etat = Etat.FINMUNUSER;
        }
    }

    public void tirerJoueur(int id, Coordonnees coord)  {
        bateauSelect = user.getBateauByID(id);
        user.tirer(coord);
        bateauSelect.tirer();
        Historique.getInstance().addAction(new ActionTirer(coord,"joueur",id));
        bateauSelect = null;

    }
    public void tirerIA(int id, Coordonnees coord)  {
            ia.tirer(coord, id);
            Historique.getInstance().addAction(new ActionTirer(coord,"machine",id));
    }

    public Tir[][] getToucher(String type) {
        if(type.equals("machine"))
            return ia.getEnsTir();
        else
            return user.getEnsTir();
    }

    public Bateau getBateauSelect() {
        return bateauSelect;
    }

    public void setBateauSelect(Bateau bateauSelect) {
        this.bateauSelect = bateauSelect;
    }

    public Bateau selectionBateau(Coordonnees coordonnees) {
        return user.getBateau(coordonnees);
    }

    public Boolean toucher(Coordonnees coordonnees, String type) {
        if(type.equals("joueur"))
            return ia.toucher(coordonnees,bateauSelect.getPuissance());
        else {
            return user.toucher(coordonnees,ia.getPuissanceTir());
        }
    }

    public void removeEmplacement() {
        emplacement.clear();
    }

    public Set<Bateau> getBateauJoueur() {
        return user.getEnsBateau();
    }

    public Set<Bateau> getBateauMachine() {
        return ia.getEnsBateau();
    }

    public void calculPosDispo(Coordonnees coordonnees) {
        emplacement.clear();
        ArrayList<Disponible> dispo = user.estDisponible();
        if (dispo.size() == 0)
            return;
        HashMap<Coordonnees,Emplacement> coord = new HashMap<>();
        for (int i = 1 ; i <= dispo.get(dispo.size()-1).getNbCase(); i++) {
            Coordonnees c = new Coordonnees(coordonnees.getX(),coordonnees.getY()-i);
            if (c.getY() >= 0 && !user.presenceBateau(c) && dispo.contains(new Disponible(i+1,0,0,0)))
                coord.put(c,new Emplacement(Direction.HAUT,i+1,dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getNbMunition(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getVie(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getPuissance()));
            else if (c.getY() < 0 || user.presenceBateau(c))
                i = dispo.get(dispo.size()-1).getNbCase();
        }
        for (int i = 1 ; i <= dispo.get(dispo.size()-1).getNbCase(); i++) {
            Coordonnees c = new Coordonnees(coordonnees.getX(),coordonnees.getY()+i);
            if (c.getY() < hauteur && !user.presenceBateau(c) && dispo.contains(new Disponible(i+1,0,0,0)))
                coord.put(c,new Emplacement(Direction.BAS,i+1,dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getNbMunition(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getVie(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getPuissance()));
            else if (c.getY() >= hauteur || user.presenceBateau(c))
                i = dispo.get(dispo.size()-1).getNbCase();
        }
        for (int i = 1 ; i <= dispo.get(dispo.size()-1).getNbCase(); i++) {
            Coordonnees c = new Coordonnees(coordonnees.getX()-i,coordonnees.getY());
            if (c.getX() >= 0 && !user.presenceBateau(c) && dispo.contains(new Disponible(i+1,0,0,0))) {
                coord.put(c, new Emplacement(Direction.GAUCHE, i + 1,dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getNbMunition(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getVie(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getPuissance()));
            } else if (c.getX() < 0 || user.presenceBateau(c)) {
                i = dispo.get(dispo.size()-1).getNbCase() + 1;
            }
        }
        for (int i = 1 ; i <= dispo.get(dispo.size()-1).getNbCase(); i++) {
            Coordonnees c = new Coordonnees(coordonnees.getX()+i,coordonnees.getY());
            if (c.getX() < largeur && !user.presenceBateau(c) && dispo.contains(new Disponible(i+1,0,0,0)))
                coord.put(c,new Emplacement(Direction.DROITE,i+1,dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getNbMunition(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getVie(),dispo.get(dispo.indexOf(new Disponible(i+1,0,0,0))).getPuissance()));
            else if (c.getX() >= largeur || user.presenceBateau(c))
                i = dispo.get(dispo.size()-1).getNbCase();
        }

        emplacement = coord;

    }

    public HashMap<Coordonnees, Emplacement> getEmplacement() {
        return emplacement;
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    public int getBateauDispo() {
        return user.estDisponible().size();
    }

    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public int getHauteur() {
        return hauteur;
    }

    public void setHauteur(int hauteur) {
        this.hauteur = hauteur;
    }

    public Bataille(Map<Integer, Integer> bateauDispo) {
        this.bateauDispo = bateauDispo;
        this.etat = Etat.EPOQUE;
        this.emplacement = new HashMap<>();
        user = new Humain(this);
        ia = new Machine(this);

        emplacement.clear();
        caseSelect = null;
        rejouer = true;
        update();
    }

    public Etat getEtat() {
        return etat;
    }

    public void changeEtat(Etat e) {
        if (etat == Etat.PREPA && e == Etat.ENJEU){
            for (Bateau b: ia.getEnsBateau()){
                Historique.getInstance().addAction(b.getAction());
            }
            for (Bateau b: user.getEnsBateau()){
                Historique.getInstance().addAction(b.getAction());
            }
            Historique.getInstance().addAction(new ActionJouer());
        }
        etat = e;
        update();
    }

    public Epoque getEpoque() {
        return epoque;
    }

    public Coordonnees getCaseSelect() {
        return caseSelect;
    }

    public void setCaseSelect(Coordonnees caseSelect) {
        this.caseSelect = caseSelect;
    }

    public void setEpoque(String epoque, boolean save){
        rejouer = false;
        this.epoque = Epoque.creerEpoque(epoque);
        Historique.getInstance().addAction(new ActionEpoque(epoque));
        etat = Etat.PREPA;
        user.addBateau(this.epoque.creerBateau());
        ia.addBateau(this.epoque.creerBateau());
        if (!save)
            ia.placementBateau();
        emplacement.clear();
        update();
    }

    public boolean estPret() {
        if(user.estDisponible().size() == 0)
            return true;
        return false;
    }

    public void placerBateauJoueur( Coordonnees coordonnees, Direction dir, int idBateau, ActionPlacerBateau a) {
        user.placerBateau(coordonnees,dir,idBateau,a);
    }

    public void placerBateauMachine( Coordonnees coordonnees, Direction dir, int idBateau, ActionPlacerBateau a) {
        ia.placerBateau(coordonnees,dir, idBateau,a);
    }

    public void effacerBateauIA() {
        ia.effacerBateau();
    }

    public void initialiser(){
        this.bateauDispo = new HashMap<>();
        Historique.getInstance().effacerHisto();
        this.etat = Etat.EPOQUE;
        this.emplacement = new HashMap<>();
        user = new Humain(this);
        ia = new Machine(this);

        emplacement.clear();
        caseSelect = null;
        rejouer = true;
        update();

    }

    public boolean isRejouer() {
        return rejouer;
    }

    public void setRejouer(boolean rejouer) {
        this.rejouer = rejouer;
    }
}
