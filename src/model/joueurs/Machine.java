package model.joueurs;

import model.Bataille;
import model.plateau.Bateau;
import model.plateau.Coordonnees;
import model.plateau.Tir;

import java.util.Random;
import java.util.Set;

/**
 * Created by Utilisateur on 07/11/2018.
 */
public class Machine extends Joueur {

    private Bateau bateauSelect;

    public Machine(Bataille mod) {
        this.mod = mod;
        type = "machine";
    }

    /*A faire */
    public Coordonnees jouer(){
        Random r = new Random();
        bateauSelect = null;
        for (Bateau b : ensBateau) {
            if(b.getMunition() > 0) {
                bateauSelect = b;
            }
        }

        while(true) {
            int x = r.nextInt(mod.getLargeur());
            int y = r.nextInt(mod.getHauteur());
            Coordonnees c = new Coordonnees(x, y);
            if (ensTir[x][y] == null)
                return c;
        }
    }

    public int getPuissanceTir() {
        return bateauSelect.getPuissance();
    }

    public Bateau getBateauSelect() {
        return bateauSelect;
    }

    @Override
    public boolean tirer(Coordonnees coord) {
        boolean tirer = super.tirer(coord);
        bateauSelect.tirer();
        return tirer;
    }



    public void placementBateau() {
        while(estDisponible().size() != 0) {
            mod.removeEmplacement();
            Random r = new Random();
            Coordonnees c = new Coordonnees(r.nextInt(mod.getLargeur()), r.nextInt(mod.getHauteur()));
            if(!presenceBateau(c)) {
                mod.selectCase(c, "machine");
                Set<Coordonnees> emplacement = mod.getEmplacement().keySet();
                if(emplacement.size() != 0) {
                    int iR = r.nextInt(emplacement.size());
                    int i = 0;
                    for (Coordonnees cEmpl : emplacement) {
                        if(i == iR)
                            placerBateau(mod.getEmplacement().get(cEmpl).getNbCase(),c,mod.getEmplacement().get(cEmpl).getDir());
                        i++;
                    }

                }
            }
        }
    }

    public void jouerAlgo1(){}
    public void jouerAlgo2(){}

    public void tirer(Coordonnees coord, int id) {
        bateauSelect = getBateauByID(id);
        tirer(coord);
    }

    public void effacerBateau() {
        for (Bateau b : ensBateau) {
            b.setPosBateau(null);
        }
    }
}
