package model.joueurs;
import model.Bataille;
import model.historique.ActionPlacerBateau;
import model.plateau.*;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Utilisateur on 07/11/2018.
 */
public abstract class Joueur {
    protected Set<Bateau> ensBateau;
    protected Tir[][] ensTir = new Tir[10][10];
    protected Bataille mod;
    protected int nbMunitions = 0;
    protected String type;

    public Set<Bateau> getEnsBateau() {
        return ensBateau;
    }

    public void addBateau(Set<Bateau> bateau){
        ensBateau = bateau;
        for (Bateau b : ensBateau)
            nbMunitions += b.getMunition();
    }

    public boolean tirer(Coordonnees coord){
        if(ensTir[coord.getX()][coord.getY()] == null) {
            nbMunitions--;
            ensTir[coord.getX()][coord.getY()] = new Tir(mod.toucher(coord,type),coord);
            return true;
        }
        return false;
    }

    public Tir[][] getEnsTir() {
        return ensTir;
    }

    public void setEnsTir(Tir[][] ensTir) {
        this.ensTir = ensTir;
    }

    public void placerBateau(int nbCase, Coordonnees coordonnees, Direction dir) {
        Bateau aPlacer = new Bateau("testCol",nbCase,0,0,0);
        Bateau dispo = estDisponible(nbCase);
        if(dispo != null) {
            aPlacer.calculPosBateau(coordonnees, dir);
            boolean colision = false;
            for (Bateau b : ensBateau) {
                if (b.collisionBateau(aPlacer)) {
                    colision = true;
                    break;
                }
            }
            if(!colision) {
                dispo.setPosBateau(aPlacer.getPosBateau());
                dispo.setAction(new ActionPlacerBateau(nbCase,dir,coordonnees,type, dispo.getId()));
                dispo.setDir(dir);
            }
        }
    }

    public Bateau estDisponible(int nbCase){
        for (Bateau b : ensBateau) {
            if(b.getNbCases() == nbCase && !b.estPlace())
                return b;
        }
        return null;
    }

    public ArrayList<Disponible> estDisponible() {
        ArrayList<Disponible> dispo = new ArrayList<>();
        for (Bateau b : ensBateau) {
            if (b.getPosBateau().size() == 0) {
                dispo.add(new Disponible(b.getNbCases(),b.getMunition(),b.getVie(),b.getPuissance()));
            }
        }
        Collections.sort(dispo);
        return dispo;
    }

    public boolean presenceBateau(Coordonnees coordonnees) {
        for(Bateau b : ensBateau) {
            if (b.estPresent(coordonnees)) {
                return true;
            }
        }
        return false;
    }

    public void retirerBateau(Coordonnees coordonnees) {
        for(Bateau b : ensBateau) {
            if (b.estPresent(coordonnees)) {
                b.setPosBateau(new ArrayList<>());
            }
        }
    }

    public int getNbMunitions() {
        return nbMunitions;
    }

    public void setNbMunitions(int nbMunitions) {
        this.nbMunitions = nbMunitions;
    }

    public Boolean toucher(Coordonnees tir, int puissance){
        Boolean toucher = false;
        Bateau couler = null;
        for(Bateau b : ensBateau) {
            if (b.estPresent(tir)) {
                b.toucher(puissance);
                if (b.getVie() <= 0) {
                    couler = b;
                }
                toucher = true;
            }
        }
        if(couler != null) {
            ensBateau.remove(couler);
            nbMunitions-=couler.getMunition();
            return null;
        }
        if(toucher)
            return toucher;

        return false;
    }


    public Bateau getBateau(Coordonnees coordonnees) {
        for (Bateau b : ensBateau) {
            if (b.estPresent(coordonnees))
                return b;
        }
        return null;

    }


    public Bateau getBateauByID(int id) {
        for (Bateau b: ensBateau){
            if (b.getId() == id){
                return b;
            }
        }
        return null;
    }

    public  void placerBateau(Coordonnees coordonnees, Direction dir, int idBateau,ActionPlacerBateau a){
        Bateau dispo = getBateauByID(idBateau);

        dispo.calculPosBateau(coordonnees,dir);
        dispo.setDir(dir);
        dispo.setId(idBateau);
        dispo.setAction(a);


    }

    public int getPdv(){
        int pdv = 0;
        for(Bateau b : ensBateau) {
            pdv += b.getVie();
        }
        return pdv;
    }
}
