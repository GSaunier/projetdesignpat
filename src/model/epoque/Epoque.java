package model.epoque;
import model.plateau.Bateau;

import java.util.*;

public abstract class Epoque {
    protected String nom;
    private static Epoque instance;

    public String getNom(){return this.nom;}

    public abstract HashSet<Bateau> creerBateau();

    public static Epoque creerEpoque(String epoque) {
        switch (epoque) {
            case "XX":
                instance = EpoqueXX.getInstance();
                break;
            case "XVI":
                instance = EpoqueXVI.getInstance();
                break;
        }
        return instance;
    }

    public static ArrayList<String> getEpoque() {
        ArrayList<String> liste = new ArrayList<>();
        liste.add("XX");
        liste.add("XVI");
        return liste;
    }

    public static Epoque getInstance() {
        return instance;
    }
}
