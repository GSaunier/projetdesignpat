package model.epoque;

import model.plateau.Bateau;

import java.util.ArrayList;
import java.util.HashSet;

public class EpoqueXX extends Epoque {

    private static EpoqueXX instance;

    private EpoqueXX(){
        this.nom="XX";
    }

    @Override
    public HashSet<Bateau> creerBateau() {
        HashSet<Bateau> liste = new HashSet<>();
        liste.add(new Bateau("Croiseur", 5,4,15,20));
        liste.add(new Bateau("Bombardier", 3,1,2,50));
        liste.add(new Bateau("Destroyer", 4,2,8,20));
        liste.add(new Bateau("Destroyer", 4,2,8,20));
        liste.add(new Bateau("Patrouilleur",2,1,1,10));
        return liste;
    }

    public static EpoqueXX getInstance() {
        if (instance == null)
            instance = new EpoqueXX();
        return instance;
    }
}
