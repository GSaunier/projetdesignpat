package model.epoque;

import model.plateau.Bateau;

import java.util.HashSet;

public class EpoqueXVI extends Epoque{
    private static EpoqueXVI instance;

    private EpoqueXVI(){
        this.nom="XVI";
    }

    @Override
    public HashSet<Bateau> creerBateau() {
        HashSet<Bateau> liste = new HashSet<>();
        liste.add(new Bateau("Galion",4,2,8,20));
        liste.add(new Bateau("Frégate", 5,3,12,15));
        liste.add(new Bateau("Navire de Guerre",3,6,5,10));
        liste.add(new Bateau("Caravelle", 2,1,4,30));
        liste.add(new Bateau("Caravelle", 2,1,4,30));
        return liste;
    }

    public static EpoqueXVI getInstance() {
        if (instance == null)
            instance = new EpoqueXVI();
        return instance;
    }
}