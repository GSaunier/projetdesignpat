package model.plateau;

import model.plateau.Direction;

public class Emplacement {
    private Direction dir;
    private int nbCase;
    private int nbMun;
    private int vie;
    private int puissance;

    public Emplacement(Direction dir, int nbCase, int nbMun, int vie, int puissance) {
        this.dir = dir;
        this.nbCase = nbCase;
        this.nbMun = nbMun;
        this.vie = vie;
        this.puissance = puissance;
    }

    public int getVie() {
        return vie;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public int getNbMun() {
        return nbMun;
    }

    public void setNbMun(int nbMun) {
        this.nbMun = nbMun;
    }

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
    }

    public int getNbCase() {
        return nbCase;
    }

    public void setNbCase(int nbCase) {
        this.nbCase = nbCase;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    @Override
    public String toString() {
        return "nbCase:"+nbCase+" dir:"+dir;
    }
}
