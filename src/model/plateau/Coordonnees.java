package model.plateau;

import java.util.Objects;

/**
 * Created by Utilisateur on 07/11/2018.
 */
public class Coordonnees {
    private int x;
    private int y;

    public Coordonnees(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordonnees(Coordonnees coordonnees) {
        this.x = coordonnees.getX();
        this.y = coordonnees.getY();
    }

    public int getX(){ return this.x;}
    public int getY(){ return this.y;}
    public void setX(int x){ this.x=x;}
    public void setY(int y){this.y=y;}


    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordonnees)) return false;
        Coordonnees that = (Coordonnees) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public String toString() {
        return x+","+y;
    }
}
