
package model.plateau;

import model.historique.ActionPlacerBateau;

import java.awt.*;
import java.util.*;

/**
 * Created by Utilisateur on 07/11/2018.
 */
public class Bateau {
    private ArrayList<Coordonnees> posBateau;
    private String nom;
    private int nbCases;
    private int puissance;
    private int munition;
    private int vie;
    private int id;
    private Color couleur;
    private Direction dir;
    private ActionPlacerBateau action;
    private static int ID = 0;

    public Bateau(String nom, int nbCases, int puissance, int vie, int munition, Direction d) {
        this.nom = nom;
        this.nbCases = nbCases;
        this.puissance = puissance;
        this.vie = vie;
        this.munition = munition;
        dir = d;
        posBateau = new ArrayList<>();
        Random rand = new Random();
        float r = rand.nextFloat() / 2f + 0.5f;
        float g = rand.nextFloat() / 2f + 0.5f;
        float b = rand.nextFloat() / 2f + 0.5f;
        couleur = new Color(r, g, b);
        id = ID;
        ID++;

    }

    public Bateau(String nom, int nbCases, int puissance, int vie, int munition) {
        this.nom = nom;
        this.nbCases = nbCases;
        this.puissance = puissance;
        this.vie = vie;
        this.munition = munition;
        posBateau = new ArrayList<>();
        Random rand = new Random();
        float r = rand.nextFloat() / 2f + 0.5f;
        float g = rand.nextFloat() / 2f + 0.5f;
        float b = rand.nextFloat() / 2f + 0.5f;
        couleur = new Color(r, g, b);
        id = ID;
        ID++;
    }

    public Bateau(String nom, int nbCases, int puissance, int vie, int munition, int i){
        this.nom=nom;
        this.nbCases=nbCases;
        this.puissance=puissance;
        this.vie=vie;
        this.munition = munition;
        posBateau = new ArrayList<>();
        this.id =i;
    }

    public Bateau(Bateau b) {
        this.nbCases=b.getNbCases();
        this.nom = b.getNom();
        this.puissance = b.getPuissance();
        this.vie = b.getVie();
        posBateau = new ArrayList<>();
        for (Coordonnees c : b.getPosBateau())
            posBateau.add(new Coordonnees(c));

    }

    public Color getCouleur() {
        return couleur;
    }

    public ActionPlacerBateau getAction() {
        return action;
    }

    public void setAction(ActionPlacerBateau action) {
        this.action = action;
    }

    public String getNom(){ return this.nom;}
    public int getNbCases(){ return this.nbCases;}
    public int getPuissance(){return this.puissance;}
    public int getVie(){ return this.vie;}
    public int getId() {return id; }
    public void setNom(String nom){this.nom=nom;}
    public void setNbCases(int nbCases){this.nbCases=nbCases;}
    public void setPuissance(int puissance){this.puissance=puissance;}
    public void setVie(int vie){this.vie=vie;}

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
    }

    /* A faire */
    public ArrayList<Coordonnees> getPosBateau(){return posBateau;}

    public void setPosBateau(ArrayList<Coordonnees> posBateau) {
        this.posBateau = posBateau;
    }

    public void addPosBateau(Coordonnees coord){
        posBateau.add(coord);
    }
    public void calculPosBateau(Coordonnees avant, Direction direction){
        dir = direction;
        int x = avant.getX();
        int y = avant.getY();
        posBateau.add(avant);
        for(int i = 0; i < nbCases-1; i++){
            switch (direction){
                case HAUT:
                    y--;
                    break;
                case BAS:
                    y++;
                    break;
                case GAUCHE:
                    x--;
                    break;
                case DROITE:
                    x++;
                    break;
                default:
                    break;
            }
            Coordonnees c = new Coordonnees(x,y);
            posBateau.add(c);
        }
    }

    public boolean estPresent(Coordonnees coordonnees) {
        return posBateau.contains(coordonnees);
    }

    public boolean collisionBateau(Bateau bateau){
        for(Coordonnees c : bateau.getPosBateau()) {
            if (posBateau.contains(c)) {
                return true;
            }
        }
        return false;
    }

    public boolean estPlace(){
        if (posBateau.size() == 0)
            return false;
        return true;
    }

    public void tirer() {
        munition--;
    }

    public void toucher(int puissance) {
        vie-= puissance;
    }

    public int getMunition() {
        return munition;
    }

    public void setMunition(int munition) {
        this.munition = munition;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Bateau copy = new Bateau(nom,nbCases,puissance,vie,munition,dir);
        for (Coordonnees c : posBateau)
            copy.posBateau.add(new Coordonnees(c));
        return copy;
    }

    public void setId(int id) {
        this.id = id;
    }
}
