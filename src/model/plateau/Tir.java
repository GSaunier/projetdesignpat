package model.plateau;

public class Tir {
    private Boolean toucher;
    private Coordonnees coord;

    public Tir(Boolean toucher, Coordonnees coord) {
        this.toucher = toucher;
        this.coord = coord;
    }

    public Boolean isToucher() {
        return toucher;
    }

    public void setToucher(boolean toucher) {
        this.toucher = toucher;
    }

    public Coordonnees getCoord() {
        return coord;
    }

    public void setCoord(Coordonnees coord) {
        this.coord = coord;
    }
}
