package model.plateau;

import java.util.Objects;

public class Disponible implements Comparable<Disponible> {
    private Integer nbCase;
    private int nbMunition;
    private int vie;
    private int puissance;

    public Disponible(Integer nbCase, int nbMunition, int vie, int puissance) {
        this.nbCase = nbCase;
        this.nbMunition = nbMunition;
        this.vie = vie;
        this.puissance = puissance;
    }

    public int getVie() {
        return vie;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public void setVie(int vie) {
        this.vie = vie;
    }

    public int getNbCase() {
        return nbCase;
    }

    public void setNbCase(int nbCase) {
        this.nbCase = nbCase;
    }

    public int getNbMunition() {
        return nbMunition;
    }

    public void setNbMunition(int nbMunition) {
        this.nbMunition = nbMunition;
    }

    @Override
    public int compareTo(Disponible o) {
        return Integer.compare(nbCase,o.nbCase);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Disponible)) return false;
        Disponible that = (Disponible) o;
        return Objects.equals(nbCase, that.nbCase);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nbCase);
    }
}
