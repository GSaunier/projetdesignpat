package model.historique;

import model.Bataille;
import model.plateau.Coordonnees;
import model.plateau.Direction;

public class ActionPlacerBateau implements ActionStrategie {

    private int nbCase;
    private Direction dir;
    private Coordonnees coordonnees;
    private String joueur;
    private int idBateau;

    public ActionPlacerBateau(int nbCase, Direction dir, Coordonnees coordonnees, String joueur, int id) {
        this.nbCase = nbCase;
        this.dir = dir;
        this.coordonnees = coordonnees;
        this.joueur = joueur;
        this.idBateau = id;
        //System.out.println(toString());
    }

    public int getNbCase() {
        return nbCase;
    }

    public void setNbCase(int nbCase) {
        this.nbCase = nbCase;
    }

    public Direction getDir() {
        return dir;
    }

    public void setDir(Direction dir) {
        this.dir = dir;
    }

    public Coordonnees getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(Coordonnees coordonnees) {
        this.coordonnees = coordonnees;
    }

    public String getJoueur() {
        return joueur;
    }

    public void setJoueur(String joueur) {
        this.joueur = joueur;
    }

    @Override
    public String toString() {
        return joueur + " a place le bateau "+ idBateau + " en "+ coordonnees + " de "+ nbCase+" case vers: " + dir;
    }

    @Override
    public String getHisto() {
        return "placer,"+joueur+","+ idBateau+ "," + nbCase+","+dir+","+coordonnees;
    }

    @Override
    public void execute(Bataille mod) {
        if (joueur.equals("joueur"))
            mod.placerBateauJoueur(coordonnees,dir,idBateau,this);
        else
            mod.placerBateauMachine(coordonnees,dir,idBateau,this);
    }
}
