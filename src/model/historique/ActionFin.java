package model.historique;

import model.Bataille;

public class ActionFin implements ActionStrategie {
    private String gagant;

    private int nbGagnant;
    private int nbPerdant;

    public ActionFin(String g, int nbg, int nbp) {
        gagant = g;
        nbGagnant = nbg;
        nbPerdant = nbp;
    }

    @Override
    public String toString() {
        return "Le gagnant est: "+gagant+ " avec " + nbGagnant +" contre " + nbPerdant+" pdv!";
    }

    @Override
    public String getHisto() {
        return "gagant,"+gagant+","+nbGagnant+","+nbPerdant;
    }

    @Override
    public void execute(Bataille mod) {

    }
}
