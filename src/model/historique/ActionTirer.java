package model.historique;

import model.plateau.Coordonnees;

import model.Bataille;

public class ActionTirer implements ActionStrategie {

    private Coordonnees coord;
    private String joueur;
    private int idBateau;


    public ActionTirer(Coordonnees c, String j, int i){
        this.coord = c;
        this.idBateau = i;
        this.joueur = j;
    }

    public Coordonnees getCoord() { return coord; }
    public String getJoueur() { return joueur; }
    public int getIdBateau() { return idBateau; }


    @Override
    public String toString() {
        return joueur + " a tire avec le bateau "+ idBateau + " en "+ coord;
    }

    @Override
    public String getHisto() {
        return "tirer,"+joueur+","+idBateau+","+coord;
    }

    @Override
    public void execute(Bataille mod) {
        if(joueur.equals("joueur"))
            mod.tirerJoueur(idBateau,coord);
        else
            mod.tirerIA(idBateau,coord);
    }
}
