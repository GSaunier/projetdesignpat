package model.historique;


import model.Bataille;

public class ActionEpoque implements ActionStrategie {

    private String epoque;

    public ActionEpoque(String epoque) {
        this.epoque = epoque;
    }

    public String getEpoque() {
        return epoque;
    }

    public void setEpoque(String epoque) {
        this.epoque = epoque;
    }

    public String toString(){
        return "epoque choisie : " + epoque;
    }

    @Override
    public String getHisto() {
        return "epoque,"+epoque;
    }

    @Override
    public void execute(Bataille mod) {
        mod.setEpoque(epoque,true);
    }
}
