package model.historique;

import model.Bataille;
import model.Etat;

public class ActionJouer implements ActionStrategie {


    @Override
    public String getHisto() {
        return "jouer";
    }

    @Override
    public void execute(Bataille mod) {
        mod.changeEtat(Etat.ENJEU);
    }

    @Override
    public String toString() {
        return "Debut de la partie!";
    }
}
