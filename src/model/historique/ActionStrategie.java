package model.historique;


import model.Bataille;

public interface ActionStrategie {
    /**
     * @return retourne [nomAction],[attribut1],...
     */
    public String getHisto();

    public void execute(Bataille mod);
}
