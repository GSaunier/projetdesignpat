package model.historique;

import java.rmi.Remote;
import java.util.ArrayList;

public interface HistoRMI extends Remote {
    public void addAction(ActionStrategie a);
    public void setActions(ArrayList<ActionStrategie> actions);
}
