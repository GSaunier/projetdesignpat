package model.historique;


import model.Bataille;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class Historique{
    private ArrayList<ActionStrategie> actions;
    private static Historique instance;

    private Historique() throws RemoteException {
        super();
        actions = new ArrayList<>();
    }

    public ArrayList<String> getHisto(int size) {
        ArrayList<String> st = new ArrayList<>();
        for (int i = size; i < actions.size(); i++){
            st.add(actions.get(i).toString());
        }
        return st;
    }

    public ArrayList<ActionStrategie> getActions() {
        return actions;
    }

    public void addAction(ActionStrategie a) {
        actions.add(a);
    }

    public void setActions(ArrayList<ActionStrategie> actions) {
        this.actions = actions;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (ActionStrategie a: actions){
            sb.append(a+"\n");
        }
        return sb.toString();
    }

    public void execute(Bataille m) {
        for (ActionStrategie a : actions) {
            a.execute(m);
        }
    }

    public static Historique getInstance() {
        if (instance == null) {
            try {
                instance = new Historique();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public void effacerHisto() {
        actions.clear();
    }
}
