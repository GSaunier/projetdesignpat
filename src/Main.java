
import model.Bataille;
import view.VueGenerale;

import javax.swing.*;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new javax.swing.plaf.metal.MetalLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        Bataille mod = new Bataille(new HashMap<Integer, Integer>());
        VueGenerale vg = new VueGenerale(mod);
    }
}
